export const types = {
    //sync
    TASK_MANAGER_FILL_TASKS: 'TASK_MANAGER_FILL_TASKS',
    //async
    TASK_MANAGER_FETCH_TASKS_ASYNC: 'TASK_MANAGER_FETCH_TASKS_ASYNC',
    TASK_MANAGER_UPDATE_TASK: 'TASK_MANAGER_UPDATE_TASK',
    TASK_MANAGER_DELTE_TASK: 'TASK_MANAGER_DELETE_TASK',
    TASK_MANAGER_CREATE_TASK: 'TASK_MANAGER_CREATE_TASK',

}