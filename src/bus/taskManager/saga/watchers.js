//Core
import { takeEvery, all, call } from 'redux-saga/effects';

//Types

//Workers
import {
    fetchTasks,
    updateTask,
    deleteTask,
    createTask
} from './workers';
import {types} from "../types";

function* watchFetchTasks() {
    yield takeEvery(types.TASK_MANAGER_FETCH_TASKS_ASYNC, fetchTasks)
}

function* watchUpdateTask() {
    yield takeEvery(types.TASK_MANAGER_UPDATE_TASK, updateTask)
}

function* watchDeleteTask() {
    yield takeEvery(types.TASK_MANAGER_DELTE_TASK, deleteTask)
}

function* watchCreateTask() {
    yield takeEvery(types.TASK_MANAGER_CREATE_TASK, createTask)
}

export function* watchTaskManager() {
    yield all(
        [
            call(watchFetchTasks),
            call(watchUpdateTask),
            call(watchDeleteTask),
            call(watchCreateTask)
        ]
    );
}
