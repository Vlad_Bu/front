//Core
import { put, call, delay } from 'redux-saga/effects';

//Other
import {taskManagerActions} from '../../actions' ;
import {api} from "../../api";

export function* updateTask(id, isCompleted) {
    try {
        const loading = true;
        const response = yield call(api.tasks.updateTaskStatus, id, isCompleted);


        yield delay(()=>{

        });

        if(response.status !== 200) {
            throw new Error('Some error updateTask');
        }


    }
    catch (error) {
           console.log('!!!!!!!!!!',error);
    } finally {

    }
}