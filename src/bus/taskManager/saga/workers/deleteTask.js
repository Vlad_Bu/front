//Core
import { put, call, delay } from 'redux-saga/effects';

//Other
import {api} from "../../api";
import {fetchTasks} from './fetchTasks';

export function* deleteTask(id) {
    try {
        const loading = true;
        const response = yield call(api.tasks.deleteTask, id);
        const result = yield call([response, response.json]);

        yield delay(()=>{

        });

        if(response.status !== 200) {
            throw new Error('Some error deleteTask');
        }

        yield* fetchTasks();

    }
    catch (error) {
           console.log('!!!!!!!!!!',error);
    } finally {

    }
}