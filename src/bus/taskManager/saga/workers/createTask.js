//Core
import { put, call, delay } from 'redux-saga/effects';

//Other

import {api} from "../../api";
import {fetchTasks} from './fetchTasks';

export function* createTask(title) {
    try {
        const response = yield call(api.tasks.createTask, title);
        const result = yield call([response, response.json]);

        yield delay(()=>{

        });

        if(response.status !== 201) {
            throw new Error('Some error createTask');
        }
            yield* fetchTasks();

    }
    catch (error) {
           console.log('!!!!!!!!!!',error);
    } finally {

    }
}