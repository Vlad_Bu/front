export const api = {
    tasks: {
        getAll: async () => {
            const response = await fetch('http://localhost:5001/tasks');

            return response;
        },
        updateTaskStatus: async ({id, isCompleted}) => {
            const response = await fetch(
                `http://localhost:5001/tasks/${id}/update`,
                {
                    method: 'PUT',
                    cache: 'no-cache',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({'isCompleted': isCompleted}),
                },
            );

            return response;
        },

        createTask: async ({title}) => {
            const response = await fetch(
                `http://localhost:5001/tasks/create`,
                {
                    method: 'POST',
                    cache: 'no-cache',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({'title': title}),
                },
            );

            return response;
        },
        deleteTask: async ({id}) => {
            console.log('deleteTask', id);
            const response = await fetch(
                `http://localhost:5001/tasks/${id}/delete`,
                {
                    method: 'Delete',
                    cache: 'no-cache',
                    headers: {'Content-Type': 'application/json'},
                },
            );

            return response;
        },
    },
};