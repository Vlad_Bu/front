// Types
import { types} from "./types";

export const taskManagerActions = {
    // sync
    fillTasks: (tasks) => {
        return {
            type: types.TASK_MANAGER_FILL_TASKS,
            payload: tasks
        }
    },
    //async
    fetchTasksAsync: () => {
       return  {type: types.TASK_MANAGER_FETCH_TASKS_ASYNC};
    },

    updateTask: (id, isCompleted) => {
        return {type: types.TASK_MANAGER_UPDATE_TASK, "id":id, "isCompleted":isCompleted};
    },

    deleteTask: (id) => {
        return {type: types.TASK_MANAGER_DELTE_TASK, "id":id};
    },

    createTask: (title) => {
        return {type: types.TASK_MANAGER_CREATE_TASK, "title":title};
    }
}