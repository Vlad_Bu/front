import React, {useState} from 'react';
import {
    DefaultPalette,
    Text,
    Spinner,
    SpinnerSize,
    Stack
} from 'office-ui-fabric-react';
import {
    TextField,
    DefaultButton,
} from '@fluentui/react';

import {useConstCallback} from '@uifabric/react-hooks';

// Elements
// Components
import {List} from './components/list';

// Hooks
import {
    useTaskManager,
    updateTask,
    deleteTask,
} from './hooks/useTaskManager';
import {taskManagerActions} from './actions';
import {useDispatch} from 'react-redux';

const stylesT = {
    root: {
        alignItems: 'center',
        background: DefaultPalette.themeDarker,
        height: 'auto',
        maxHeight: '150px',
        padding: '10px',
        color: DefaultPalette.redDark,
    },
};

export const TaskManager = () => {
    const dispatch = useDispatch();
    const {tasks} = useTaskManager();
    const [textFieldValue, setTextFieldValue] = useState('');
    const textFieldStyles = {fieldGroup: {width: 300}};

    const handleSubmit = () => {
         dispatch(taskManagerActions.createTask(textFieldValue));
    };

    const onChangeTextFieldValue = useConstCallback(
        (event, newValue) => {
            setTextFieldValue(newValue || '');
        },
    );
    const InputValue = {
        value: '',
    };

    const stylesRow = {
        width: '50%',
        margin: '0 auto',

    };

    return (
        <>
            <div className="ms-Grid-row"
                 style={stylesRow}>
                <Text variant="xLarge"
                      style={stylesT}>
                    Task Manager
                </Text>
            </div>
            <div className="ms-Grid-row">
                <TextField
                    label={InputValue.label}
                    value={textFieldValue}
                    onChange={onChangeTextFieldValue}
                    styles={textFieldStyles}

                />
                <DefaultButton text="Create new task"
                               onClick={handleSubmit} />

            </div>
            <div className="ms-Grid-row">
                <List items={tasks} />
            </div>
            <br />
        </>
    );
};