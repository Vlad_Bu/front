//tpyes
import { types } from "./types";

const initialState = {
    tasks: []
}
export const taskManagerReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.TASK_MANAGER_FILL_TASKS:
            return {
                ...state,
                tasks: action.payload
            }
        case types.TASK_MANAGER_UPDATE_TASK:
        case types.TASK_MANAGER_DELTE_TASK:
        case types.TASK_MANAGER_CREATE_TASK:

            return state;

        default:
            return state;
    }
}