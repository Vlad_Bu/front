import React from 'react';

// Elements
import {Task} from '../../../../elements/task';



export const List = ({items}) => {
    const listJSX = items.map((
                                  {
                                      id,
                                      title,
                                      isCompleted,
                                  },
                              ) => (
        <>
            <p>
                <Task
                    id={id}
                    label={title}
                    isCompleted={isCompleted}
                />
            </p>
            <br />
        </>
    ));

    return (
        <>
            {listJSX}
        </>
    );
};