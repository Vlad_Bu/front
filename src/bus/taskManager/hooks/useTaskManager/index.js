// Core
import {
    useEffect,
    useState,
} from 'react';

// API
import {api} from '../../api';
import {useDispatch, useSelector } from "react-redux";
import {taskManagerActions} from '../../actions';

// Other

export const useTaskManager = () => {
    const tasks = useSelector((state) => state.taskManager.tasks);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(taskManagerActions.fetchTasksAsync());
    }, [dispatch]);

    return {
        tasks,

    };
};

