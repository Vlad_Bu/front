import React, {useState} from 'react';
import {
    Checkbox,
    Stack,
    DefaultButton,
} from '@fluentui/react';
//import { updateTask, deleteTask } from '../../bus/taskManager/hooks/useTaskManager';
import {DefaultPalette} from 'office-ui-fabric-react/lib/Styling';
import {useDispatch} from 'react-redux';
import {taskManagerActions} from '../../bus/taskManager/actions';


export const Task = ({id, isCompleted, label}) => {
    const dispatch = useDispatch();
    const [checkedStatus, setChecked] = useState(isCompleted);
    const [textFieldValue, setTextFieldValue] = useState('');
    const textFieldStyles = {fieldGroup: {width: 300}};

    const handleCheck = () => {
        isCompleted = (!checkedStatus);
        setChecked(isCompleted);

        dispatch(taskManagerActions.updateTask(id, isCompleted));
    };

    const handleDelete = () => {
        dispatch(taskManagerActions.deleteTask(id));
    };

    const ButtonValue = {
        label: 'Delete',
        value: '',
    };

    const stackTokens = {childrenGap: 40};
    const stackStyles = {
        'root': {
            background: DefaultPalette.themeLight,
            color: DefaultPalette.white,
            padding: 5,
            maxWidth: '500px',
        },
    };

    return (
        <>
            <Stack horizontal
                   tokens={stackTokens}
                   styles={stackStyles}>
                <Checkbox
                    label={label}
                    checked={checkedStatus}
                    onChange={handleCheck}

                />
                <DefaultButton text={ButtonValue.label}
                               onClick={handleDelete} />
            </Stack>
            <br />
        </>
    );
};