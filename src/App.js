import * as React from 'react';
import { DefaultPalette, Stack ,Text } from 'office-ui-fabric-react';
import { TaskManager } from './bus/taskManager';
import {Provider} from 'react-redux';
import {store} from "./init/store";


// Styles definition
const taskHeadStyles =  {
  root: {
    alignItems: 'center',
    background:  DefaultPalette.themeLight,
    height: 'auto',
    maxHeight: '150px',
    padding: '10px',
    color: DefaultPalette.redDark,
  },
};

const titleStyle = {
  root: {
    horizontalAlign: 'center',
  }
};

export const App = () => (
  <>
      <Provider store={store}>
            <div className="ms-Grid-row" >
                  <Stack class="ms-Grid-col ms-sm12 ms-lg12" styles={taskHeadStyles}>
                  <Text variant="mega" style={titleStyle}>
                        Task App
                      </Text>
                  </Stack>
            </div>
        <TaskManager />
      </Provider>
  </>
);



  